<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuadro extends Model
{
    //
    protected $table = 'pintores';
	protected $primaryKey = 'id';
	public $timestamps = false;
}

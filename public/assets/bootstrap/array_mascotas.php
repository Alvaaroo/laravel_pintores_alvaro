private $arrayMascotas = array(
		array(
			'nombre' => 'Kira',
			'especie' => 'Perro',
			'raza' => 'Galgo',
			'fechaNacimiento' => '2014-09-07',
			'imagen' => 'kira.jpg',
			'historial' => 'Vacunas al día',
			'cliente' => 'Iván'
		),
		array(
			'nombre' => 'Conejo',
			'especie' => 'Conejo',
			'raza' => 'Enano',
			'fechaNacimiento' => '2011-07-07',
			'imagen' => 'conejo.jpg',
			'historial' => 'Limado de dientes. Vacunas al día',
			'cliente' => 'Pepita'
		),
		array(
			'nombre' => 'Gato',
			'especie' => 'Gato',
			'raza' => 'Siamés',
			'fechaNacimiento' => '2016-05-16',
			'imagen' => 'gato.jpg',
			'historial' => 'Todo bien',
			'cliente' => 'Pepe'
		),
	);
